export function getProducts() {
  return dispatch => {
    let options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    };
    fetch(
      'https://assignment-appstreet.herokuapp.com/api/v1/products?page=1',
      options
    )
      .then(res => {
        return res.json();
      })
      .catch(error => console.log('error', error))
      .then(result => {
        result.page = 1;

        dispatch(saveProducts(result));
      });
  };
}

export function addMoreProducts(page) {
  return (dispatch, getState) => {
    let olderProduct = [];
    olderProduct = getState().ProductListReducer;
    let options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    };
    fetch(
      `https://assignment-appstreet.herokuapp.com/api/v1/products?page=${page}`,
      options
    )
      .then(res => res.json())
      .catch(error => console.log('error', error))
      .then(({ products }) => {
        if (products && products.length) {
          olderProduct.products.push(...products);
          olderProduct.page = page;
          dispatch(saveProducts(olderProduct));
          // success();
        } else {
          // failure();
        }
        // dispatch(savePlanets(planets));
      });
  };
}

function saveProducts(products) {
  return { type: 'SAVE_PRODUCTS', products: { ...products } };
}
