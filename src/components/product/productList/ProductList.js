import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getProducts, addMoreProducts } from './ProductListAction';
import SingleProduct from './SingleProduct';
import InfiniteScroll from 'react-infinite-scroller';
import pathOr from 'lodash/fp/pathOr';
import './ProductList.css';
class ProductList extends Component {
  constructor(props) {
    super(props);
    this.loadMore = this.loadMore.bind(this);
    this.state = {
      hasMore: false
    };
  }
  componentWillReceiveProps(nextProps) {
    const productsList = pathOr([], 'productsList', nextProps);
    if (productsList.length) {
      this.setState({ hasMore: true });
    }
  }
  componentDidMount() {
    this.props.dispatch(getProducts());
  }

  loadMore() {
    let page = pathOr(1, 'page', this.props);
    this.props.dispatch(addMoreProducts(page + 1));
  }
  render() {
    let productArray = this.props.productsList || [];

    return (
      <InfiniteScroll
        pageStart={0}
        loadMore={this.loadMore}
        hasMore={this.state.hasMore}
      >
        <div className="productBody">
          {productArray.map((doc, index) => {
            return <SingleProduct key={index} index={index} product={doc} />;
          })}
        </div>
      </InfiniteScroll>
    );
  }
}

var mapStateToProps = state => {
  if (state && state.ProductListReducer && state.ProductListReducer) {
    return {
      page: state.ProductListReducer.page,
      productsList: state.ProductListReducer.products
    };
  }
};

ProductList = connect(mapStateToProps)(ProductList);

export default ProductList;
