import React, { Component } from 'react';
import './ProductList.css';
import { Link } from 'react-router-dom';
class SingleProduct extends Component {
  render() {
    let product = {};
    if (this.props.product) {
      product = this.props.product;
    }
    return (
      <Link to={`/product-detail/${product._id}`}>
        <div className="singleProduct">
          <div className="productPicture">
            <img src={product.images && product.images[0]} />
          </div>
          <div className="productName">{product.name}</div>
          <div className="productPrice">
            <b>Rs.{product.mark_price}</b>
          </div>
        </div>
      </Link>
    );
  }
}

export default SingleProduct;
