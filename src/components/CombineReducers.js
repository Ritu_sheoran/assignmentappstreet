import ProductListReducer from "./product/productList/ProductListReducer";
import ProductDetailReducer from "./product/productDetail/ProductDetailReducer";

const ReducerExport = {
  ProductListReducer,
    ProductDetailReducer
};

export default ReducerExport;
