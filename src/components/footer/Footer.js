import React, { Component } from "react";
import "./Footer.css";

export default class Footer extends Component {
  render() {
    return (

      <div className="footerParent">
      <div className="footer">
        <div className="footerLeft">
          <div className="footerAbout">About |</div>
          <div className="footerContact">Contact |</div>
          <div className="footerPrivacyPolicy">Privacy Policy |</div>
          <div className="footerReturnPolicy">Return Policy</div>
        </div>
      </div>
      </div>
    );
  }
}
